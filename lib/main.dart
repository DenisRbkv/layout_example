import 'package:flutter/material.dart';

import 'package:device_simulator/device_simulator.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:layout_example/test_container.dart';
import 'package:layout_example/text_with_padding.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DeviceSimulator(
        brightness: Brightness.dark,
        enable: true,
        child: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _flex = 2;

  bool _isAxisAlignCenter = false;
  MainAxisAlignment _mainAxisAlignment = MainAxisAlignment.spaceBetween;

  bool _isScrUtilHasOnlyWidth = false;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      designSize: Size(375.0, 812.0),
      allowFontScaling: false,
    );
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 20.0,
          vertical: 80.0, //40
        ),
        child: Column(
          children: <Widget>[
            Text('Flex-approach. Horizontal flex $_flex'),
            const SizedBox(height: 10.0),
            Expanded(
              flex: 2,
              child: InkWell(
                onTap: _changeFlex,
                splashColor: Colors.transparent,
                child: Row(
                  children: [
                    const Spacer(),
                    Expanded(
                      flex: _flex,
                      child: TestContainer(
                        color: Colors.yellowAccent,
                        height: null,
                      ),
                    ),
                    const Spacer(),
                    Expanded(
                      flex: _flex,
                      child: TestContainer(
                        color: Colors.deepOrange,
                        height: null,
                      ),
                    ),
                    const Spacer(),
                  ],
                ),
              ),
            ),
            TextWithPadding('Static + AxisAlignment: $_mainAxisAlignment'),
            InkWell(
              onTap: _getAxisAlignment,
              splashColor: Colors.transparent,
              child: Row(
                mainAxisAlignment: _mainAxisAlignment,
                children: [
                  TestContainer(color: Colors.deepOrange),
                  TestContainer(color: Colors.yellow),
                ],
              ),
            ),
            TextWithPadding('ScreenUtil: ${_isScrUtilHasOnlyWidth ? 'all .w' : '.w, .h'}'),
            InkWell(
              splashColor: Colors.transparent,
              onTap: _updScreenUtil,
              child: _getScreenUtilWidget,
            ),
            TextWithPadding('LayoutBuilder'),
            LayoutBuilder(
              builder: (context, constraints) {
                if (constraints.maxWidth > 500.0) {
                  return TestContainer(
                    width: 400.0,
                    height: 350.0,
                    text: 'HUGE',
                  );
                }
                if (constraints.maxWidth > 350.0) {
                  return TestContainer(
                    width: 250.0,
                    height: 150.0,
                    text: 'BIG',
                  );
                }
                return TestContainer(text: 'SMALL');
              },
            ),
            const Spacer(),
          ],
        ),
      ),
    );
  }

  Widget get _getScreenUtilWidget {
    if (_isScrUtilHasOnlyWidth) {
      return TestContainer(
        width: 200.w,
        height: 100.h,
      );
    }
    return TestContainer(
      width: 200.w,
      height: 100.w,
    );
  }

  void _changeFlex() {
    if (_flex == 5) _flex = 1;
    _flex++;
    setState(() {});
  }

  void _getAxisAlignment() {
    if (_isAxisAlignCenter) {
      _mainAxisAlignment = MainAxisAlignment.spaceBetween;
    } else {
      _mainAxisAlignment = MainAxisAlignment.center;
    }
    _isAxisAlignCenter = !_isAxisAlignCenter;
    setState(() {});
  }

  void _updScreenUtil() {
    _isScrUtilHasOnlyWidth = !_isScrUtilHasOnlyWidth;
    setState(() {});
  }
}
