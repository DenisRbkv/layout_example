import 'package:flutter/material.dart';

class TextWithPadding extends StatelessWidget {
  final String text;

  TextWithPadding(this.text);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 20.0,
        bottom: 10.0,
      ),
      child: Text(text),
    );
  }
}
